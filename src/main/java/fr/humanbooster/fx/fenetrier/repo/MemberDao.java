package fr.humanbooster.fx.fenetrier.repo;

import java.util.List;

import fr.humanbooster.fx.fenetrier.domain.Member;

public interface MemberDao
{
    public Member findById(Long id);

    public Member findByEmail(String email);

    public List<Member> findAllOrderedByName();

    public void register(Member member);
}
